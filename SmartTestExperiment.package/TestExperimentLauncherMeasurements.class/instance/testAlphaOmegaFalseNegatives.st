running
testAlphaOmegaFalseNegatives
	"self run: #testAlphaOmegaFalseNegatives"

	self assert: measurements falseNegatives equals: 0.
	measurements incFalseNegatives: 3.
	self assert: measurements falseNegatives equals: 3.
	self assert: (measurements incFalseNegatives: 5) equals: 8.
	self assert: measurements falseNegatives equals: 8.
	self assert: (measurements valueAtColumn: #falseNegatives) equals: 8