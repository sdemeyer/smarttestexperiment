initialize - release
initialize
	"self new run"

	super initialize.
	changeList := {}.
	packages := {}.
	logFileReference := 'SmartTestLogFile.txt' asFileReference.
	smarttestResults := IdentityDictionary new.
	mapMethodundertestToTestcase := IdentityDictionary new.
	measurements := ExperimentLauncherMeasurements new.
	"From here onward these are all instance variables to store intermediate results"
	currentChange := nil.
	classesUnderTest := nil.
	testCaseClasses := nil.
	testResources := nil.
	testSuite := nil