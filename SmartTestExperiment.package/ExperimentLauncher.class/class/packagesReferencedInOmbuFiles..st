utilities
packagesReferencedInOmbuFiles: aListOfFileNames
	"self packagesReferencedInOmbuFiles: {
		'ForwardChainer/ForwardChainer2.ombu' .
		'ForwardChainer/ForwardChainer3.ombu' .
		'ForwardChainer/ForwardChainer4.ombu' .
		'ForwardChainer/ForwardChainer5.ombu' .
		}"

	| fileRef packageNames packageChanges |
	packageNames := Set new.
	aListOfFileNames
		do: [ :fileName | 
			fileRef := fileName asFileReference.
			fileRef exists
				ifTrue: [ packageChanges := (EpLog freshFromFile: fileName asFileReference) entries collect: [ :om | om content ].
					packageNames addAll: (packageChanges select: [ :anEpChange | anEpChange isEpCategoryChange ] thenCollect: [ :anEpChange | anEpChange affectedPackageName ]) ] ].
	^ packageNames