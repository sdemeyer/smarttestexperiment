logging
logTrueNegatives: aSetOfMethodSelectors
	aSetOfMethodSelectors isEmpty
		ifTrue: [ self log: 'No true negatives' crBefore: true indentation: 3 ]
		ifFalse: [ measurements incTrueNegatives: aSetOfMethodSelectors size. self log: '++ true negatives >> ' , aSetOfMethodSelectors size printString crBefore: true indentation: 3.
			self log: aSetOfMethodSelectors printString crBefore: true indentation: 4 ]