initialize - release
resetCollectingResults
	smarttestResults := IdentityDictionary new.
	mapMethodundertestToTestcase := IdentityDictionary new.
	measurements put: measurements rowNumber atColumn: #id.
	measurements put: currentChange methodAffected fullName atColumn: #change.
	measurements put: (currentChange class name allButFirst: 2) atColumn: #changeType