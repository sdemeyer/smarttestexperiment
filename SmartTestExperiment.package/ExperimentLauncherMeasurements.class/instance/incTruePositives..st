public
incTruePositives: withAmount
	self inc: withAmount atColumn: #truePositives.
	^ truePositives := truePositives + withAmount