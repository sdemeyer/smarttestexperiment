scenarios
warmup
	"I rely on the UImanager to give feedback. However, right after system start-up, the UI manager doesn't open the feedback window, hence this warmup."
	"self warmup"

	UIManager default
		informUserDuring: [ :bar | 
			#('one' 'two' 'three')
				do: [ :info | 
					bar label: info.
					1 to: 100 do: [ :v | 
						bar current: v.
						(Delay forMilliseconds: 5) wait ] ] ]