scenarios
runCreationOfOracle
	"self runCreationOfOracle"

	| el |
	el := self onOmbuFileNamed: 'PharoLANTest.ombu' withPackages: {'LANSimulation-Internals' . 'LANSimulation-Tests' . 'LANSimulation'}.
	^ el createTestcases; createOracle