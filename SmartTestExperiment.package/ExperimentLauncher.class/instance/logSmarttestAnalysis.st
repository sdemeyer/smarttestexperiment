logging
logSmarttestAnalysis
	self log: '** smarttest analysis >> ' crBefore: true indentation: 1.
	self log: 'smarttestResults: ---' crBefore: true indentation: 2.
	smarttestResults
		keysAndValuesDo: [ :anEpChange :aCollectionOfMethodReferences | 
			self log: anEpChange methodAffected fullName crBefore: true indentation: 3.
			self log: aCollectionOfMethodReferences printString crBefore: true indentation: 4 ]