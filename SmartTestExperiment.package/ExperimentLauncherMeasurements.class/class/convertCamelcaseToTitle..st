utilities
convertCamelcaseToTitle: aString
	"Replaces all uppercase character in aString with a space followed by the lowercase equivalent. If I encounter underscores replace them with a space as well"

	"Array with: (self convertCamelcaseToTitle: #truePositives)
		with: (self convertCamelcaseToTitle: #true_Positives)
		with: (self convertCamelcaseToTitle: '')
		with: (self convertCamelcaseToTitle: 'AAAA')"
	"(self convertCamelcaseToTitle: 3)"

	| str |
	self assert: aString isString description: 'Expected a string'.
	str := WriteStream on: (String new: aString size * 2).
	aString
		do: [ :char | 
			char isUppercase
				ifTrue: [ str
						nextPut: Character space;
						nextPut: char asLowercase ]
				ifFalse: [ char = $_
						ifTrue: [ str nextPut: Character space ]
						ifFalse: [ str nextPut: char ] ] ].
	^ str contents