public
incTrueNegatives: withAmount
	self inc: withAmount atColumn: #trueNegatives.
	^ trueNegatives := trueNegatives + withAmount