private
createWrappersForClassesUnderTest: listOfClassesUnderTest
| wr |
	wr := OrderedCollection new.
	listOfClassesUnderTest
		do: [ :classUnderTest | 
			wr addAll: classUnderTest localMethods.
			wr addAll: classUnderTest class localMethods ].
	wr := wr collect: [ :each | TestCoverage on: each methodReference ].
	^ wr