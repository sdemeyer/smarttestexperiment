private
DEPRECATEDswitchCommentChangeWithClassAddition
	"There is a little bug in Epicea files; the class comment event is done before the creation of a class, which results in a 'Class not found notification'. To prevent this, this little auxiliary method goes over them and switches their order"
	
	"THIS FUNCTIONALITY IS DEPRECATED AND IS MOVED INTO THE CLASS UTILITY TO FIX EPICEA FILES BEFORE RUNNING THEM THROUGH THE LAUCHER"

	1 to: changeList size - 1 do: [ :inx | 
		(changeList at: inx) class == EpBehaviorCommentChange
			ifTrue: [ (changeList at: inx + 1) class == EpClassAddition
					ifTrue: [ | tmp |
						Transcript
							cr;
							show: 'Switched ' , (changeList at: inx) printString , ' with ' , (changeList at: inx + 1) printString.
						tmp := changeList at: inx.
						changeList at: inx put: (changeList at: inx + 1).
						changeList at: inx + 1 put: tmp ] ] ].
	changeList inspect