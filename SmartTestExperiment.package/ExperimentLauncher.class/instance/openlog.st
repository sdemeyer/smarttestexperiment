logging
openlog
	logFileReference exists
		ifTrue: [ logFileReference delete ].
		logstream := logFileReference writeStream.