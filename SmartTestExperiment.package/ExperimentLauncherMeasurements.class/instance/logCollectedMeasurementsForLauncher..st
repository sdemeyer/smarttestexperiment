public
logCollectedMeasurementsForLauncher: anExperimentLauncher
	anExperimentLauncher log: '-------------------------------------------------------------------' crBefore: true indentation: 0.
	anExperimentLauncher log: '#truePositives = ' , truePositives printString crBefore: true indentation: 0.
	anExperimentLauncher log: '#falsePositives = ' , falsePositives printString crBefore: true indentation: 0.
	anExperimentLauncher log: '#falseNegatives = ' , falseNegatives printString crBefore: true indentation: 0.
		anExperimentLauncher log: '#trueNegatives = ' , trueNegatives printString crBefore: true indentation: 0