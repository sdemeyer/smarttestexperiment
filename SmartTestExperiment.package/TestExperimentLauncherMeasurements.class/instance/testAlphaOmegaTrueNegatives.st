running
testAlphaOmegaTrueNegatives
	"self run: #testAlphaOmegaTrueNegatives"

	self assert: measurements trueNegatives equals: 0.
	measurements incTrueNegatives: 2.
	self assert: measurements trueNegatives equals: 2.
	self assert: (measurements incTrueNegatives: 3) equals: 5.
	self assert: measurements trueNegatives equals: 5.
	self assert: (measurements valueAtColumn: #trueNegatives) equals: 5.
