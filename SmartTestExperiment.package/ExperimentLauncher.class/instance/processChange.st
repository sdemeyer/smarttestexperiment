private
processChange
	| smarttestAnalysis |
	currentChange isCodeChange
		ifTrue: [ self logAppliedCodeChange.
			currentChange applyCodeChange ]
		ifFalse: [ self logIgnoreCodeChange ].
	"If it is not a code change it is loading and saving of Moticello versions, hence can be ignored. Don't know what happens with refactorings."
	self smarttestInterventionNeeded
		ifTrue: [ smarttestAnalysis := SmTFinderSettings finder new methodsFor: currentChange methodAffected compiledMethod.
			smarttestAnalysis := smarttestAnalysis collect: [ :method | method fullName ].
			smarttestResults at: currentChange put: smarttestAnalysis.
			self logSmarttestAnalysis ]
		ifFalse: [ self log: 'No smarttest analysis needed' crBefore: true indentation: 1 ]