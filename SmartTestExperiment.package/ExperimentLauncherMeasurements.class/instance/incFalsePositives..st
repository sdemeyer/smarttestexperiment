public
incFalsePositives: withAmount
	self inc: withAmount atColumn: #falsePositives.
	^ falsePositives := falsePositives + withAmount