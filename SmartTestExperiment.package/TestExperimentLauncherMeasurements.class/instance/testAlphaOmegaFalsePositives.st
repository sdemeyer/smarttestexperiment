running
testAlphaOmegaFalsePositives
	"self run: #testAlphaOmega"

	self assert: measurements falsePositives equals: 0.
	measurements incFalsePositives: 4.
	self assert: measurements falsePositives equals: 4.
	self assert: (measurements incFalsePositives: 4) equals: 8.
	self assert: measurements falsePositives equals: 8.
	self assert: (measurements valueAtColumn: #falsePositives) equals: 8.
