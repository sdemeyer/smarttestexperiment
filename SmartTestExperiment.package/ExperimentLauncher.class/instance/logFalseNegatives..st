logging
logFalseNegatives: aSetOfMethodSelectors
	aSetOfMethodSelectors isEmpty
		ifTrue: [ self log: 'No false negatives' crBefore: true indentation: 3 ]
		ifFalse: [
			measurements incFalseNegatives: aSetOfMethodSelectors size.
			self log: '!! false negatives >> ' , aSetOfMethodSelectors size printString crBefore: true indentation: 3.
			self log: aSetOfMethodSelectors printString crBefore: true indentation: 4 ]