logging
logStartOfCodeChange
	self log: '-------------------------------------------------------------------' crBefore: true indentation: 0.
	self log: currentChange printString crBefore: true indentation: 0.
	currentChange isEpMethodChange
		ifTrue: [ self log: currentChange methodAffected sourceCode crBefore: true indentation: 0.
			self log: '...................................................................' crBefore: true indentation: 0 ]