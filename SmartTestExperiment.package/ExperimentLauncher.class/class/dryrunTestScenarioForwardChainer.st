scenarios
dryrunTestScenarioForwardChainer
	"self dryrunTestScenarioForwardChainer"

	| el |
	el := self
		onOmbuFiles:
			{'ForwardChainer/ForwardChainer2_FX.ombu' . 'ForwardChainer/ForwardChainer3.ombu' . 'ForwardChainer/ForwardChainer4.ombu'.
			'ForwardChainer/ForwardChainer5.ombu'}
		logFileName: 'ForwardChainer_LOG'
		withPackages: {#IABasic . #ConfigurationOfCloudforkAWS}.
	^ el
		prepare;
		dryrun;
		terminate