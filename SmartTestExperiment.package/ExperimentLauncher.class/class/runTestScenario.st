scenarios
runTestScenario
	"self runTestScenario"

	| el |
	el := self onOmbuFileNamed: 'PharoLANTest.ombu' withPackages: {'LANSimulation-Internals' . 'LANSimulation-Tests' . 'LANSimulation'}.
	^ el
		prepare;
		run; terminate 