logging
logCreatedTestcases
	self log: '** created oracle >> ' crBefore: true indentation: 1.
	self log: 'Classes under test: ' , classesUnderTest printString crBefore: true indentation: 2.
	self log: 'Testcases: ' , testCaseClasses printString crBefore: true indentation: 2.
	self log: 'TestResources: ' , testResources printString crBefore: true indentation: 2