running
testAlphaOmegaTruePositives
	"self run: #testAlphaOmegaTruePositives"

	self assert: measurements truePositives equals: 0.
	measurements incTruePositives: 2.
	self assert: measurements truePositives equals: 2.
	self assert: (measurements incTruePositives: 2) equals: 4.
	self assert: measurements truePositives equals: 4.
	self assert: (measurements valueAtColumn: #truePositives) equals: 4