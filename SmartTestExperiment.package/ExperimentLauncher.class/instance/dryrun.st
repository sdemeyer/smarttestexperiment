actions-public
dryrun
	"self dryrunTestScenario"

	UIManager default
		displayProgress: 'Dry run Smart Test Experiment'
		from: 1
		to: changeList size * 2
		during: [ :bar | 
			changeList
				do: [ :anEpCodeChange | 
					currentChange := anEpCodeChange.
					bar
						increment;
						label: 'Loading change for ' , anEpCodeChange printString.
					self processChangeDryRun.
					bar
						increment;
						label: 'Execute oracle for ' , anEpCodeChange printString.
					self
						createTestcases;
						runSuite ] ]