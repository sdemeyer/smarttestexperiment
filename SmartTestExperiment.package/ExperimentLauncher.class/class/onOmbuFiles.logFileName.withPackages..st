instance creation
onOmbuFiles: aListOfFileNames logFileName: aLogFileName withPackages: packageNames
	"self onOmbuFiles: {
		'ForwardChainer/ForwardChainer2.ombu' .
		'ForwardChainer/ForwardChainer3.ombu' .
		'ForwardChainer/ForwardChainer4.ombu' .
		'ForwardChainer/ForwardChainer5.ombu' .
		} logFileName: 'ForwardChainer_LOG'
	withPackages: {#IABasic . #ConfigurationOfCloudforkAWS}"

	| logfileRef changes fileRef |
	changes := OrderedCollection new.
	aListOfFileNames
		do: [ :fileName | 
			fileRef := fileName asFileReference.
			fileRef exists
				ifTrue: [ changes addAll: ((EpLog freshFromFile: fileName asFileReference) entries collect: [ :om | om content ]) ] ].
	logfileRef := aLogFileName asFileReference.
	lastRun := self new
		initialize;
		packages: packageNames;
		logFileReference: logfileRef;
		changeList: changes;
		yourself.
	^ lastRun