initalize-release
initialize: nrOfElements
	"self new: 10"

	super initialize.
	measurements := OrderedCollection new: nrOfElements.
	columnNames := {#id . #truePositives . #falsePositives . #falseNegatives . #trueNegatives . #change . #changeType}.
	measurements add: self createNewRow.
	truePositives := 0.
	falsePositives := 0.
	falseNegatives := 0.
	trueNegatives := 0