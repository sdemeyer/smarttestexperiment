scenarios
dryrunTestScenario
	"self dryrunTestScenario"

	| el |
	el := self onOmbuFileNamed: 'PharoLANTest.ombu' withPackages: {'LANSimulation-Internals' . 'LANSimulation-Tests' . 'LANSimulation'}.
	^ el
		prepare;
		dryrun;
		terminate