private
addToTestResultCovered: methodReference byTestcase: testCaseClass selector: selector
	| methodUnderTest testCase |
	methodUnderTest := methodReference fullName.
	testCase := (testCaseClass methodDict at: selector) methodReference fullName.
	(mapMethodundertestToTestcase includesKey: methodUnderTest)
		ifTrue: [ (mapMethodundertestToTestcase at: methodUnderTest) add: testCase ]
		ifFalse: [ mapMethodundertestToTestcase at: methodUnderTest put: (Set with: testCase) ]