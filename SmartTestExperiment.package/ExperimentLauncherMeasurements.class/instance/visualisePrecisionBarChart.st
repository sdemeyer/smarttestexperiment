software visualization
visualisePrecisionBarChart
	| b |
	b := RTGrapher new.
	b
		add:
			(self
				createChartData: (measurements collect: [ :row | (row at: 2) + (row at: 3) + (row at: 4) + (row at: 5) ])
				label: 'true negatives'
				color: Color blue).
	b
		add:
			(self
				createChartData: (measurements collect: [ :row | (row at: 2) + (row at: 3) + (row at: 4) ])
				label: 'false negatives'
				color: Color red).
	b
		add:
			(self
				createChartData: (measurements collect: [ :row | (row at: 2) + (row at: 3) ])
				label: 'false positives'
				color: Color yellow).
	b
		add:
			(self
				createChartData: (measurements collect: [ :row | row at: 2 ])
				label: 'true positives'
				color: Color green).
	b axisX
		title: 'System Evolution';
		noDecimal;
		noTick.
	b axisY
		title: '# Tests';
		noDecimal.
	b legend right.
	b inspect