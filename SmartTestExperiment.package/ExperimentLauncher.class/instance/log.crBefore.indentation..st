logging
log: aLogEntry crBefore: crBefore indentation: indentLevel
	(logstream notNil and: [ logstream isStream ])
		ifFalse: [ ^ self ].
	crBefore
		ifTrue: [ logstream crtab:  indentLevel].
	logstream
		nextPutAll: aLogEntry;
		flush