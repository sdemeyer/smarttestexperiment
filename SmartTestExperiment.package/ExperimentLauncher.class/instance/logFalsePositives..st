logging
logFalsePositives: aSetOfMethodSelectors
	aSetOfMethodSelectors isEmpty
		ifTrue: [ self log: 'No false positives' crBefore: true indentation: 3 ]
		ifFalse: [ measurements incFalsePositives: aSetOfMethodSelectors size. self log: '?? false positives >> ' , aSetOfMethodSelectors size printString crBefore: true indentation: 3.
			self log: aSetOfMethodSelectors printString crBefore: true indentation: 4 ]