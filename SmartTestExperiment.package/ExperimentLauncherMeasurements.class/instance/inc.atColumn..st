public
inc: withAmount atColumn: columnName
	| col row |
	self assert: (columnNames includes: columnName) description: 'Wrong columnName ' , columnName printString.
	col := columnNames indexOf: columnName.
	row := measurements last.
	row at: col put: (row at: col) + withAmount.
	^ row at: col