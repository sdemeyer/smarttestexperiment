accessing
changeList: aCollectionOfEpCodeChange
	aCollectionOfEpCodeChange assert: aCollectionOfEpCodeChange isSequenceable description: 'Expected a collection for #changeList:'.
	changeList := aCollectionOfEpCodeChange