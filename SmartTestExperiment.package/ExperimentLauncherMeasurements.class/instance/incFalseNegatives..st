public
incFalseNegatives: withAmount
	self inc: withAmount atColumn: #falseNegatives.
	^ falseNegatives := falseNegatives + withAmount