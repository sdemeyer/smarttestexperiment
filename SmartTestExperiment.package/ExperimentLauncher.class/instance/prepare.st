actions-public
prepare
	packages do: [ :aCategory | SystemOrganization removeSystemCategory: aCategory asString ].
	SmTTestFinderStrategy resetMethodToTestCache.
	self openlog