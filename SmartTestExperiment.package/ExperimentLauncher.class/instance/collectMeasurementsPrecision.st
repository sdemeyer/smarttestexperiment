private
collectMeasurementsPrecision
	| smarttestAdvice oracle allTests |
	"currentChange methodAffected fullName = #'Packet>>message:'
		ifTrue: [ self halt ]."
	self logCollectingMeasurementsPrecision.
	smarttestAdvice := IdentitySet new.
	oracle := IdentitySet withAll: (mapMethodundertestToTestcase at: currentChange methodAffected fullName ifAbsent: {}).
	smarttestResults
		keysAndValuesDo: [ :anEpChange :aCollectionOfMethodReferences | 
			smarttestAdvice addAll: aCollectionOfMethodReferences].
	allTests := IdentitySet new.
	testCaseClasses do: [ :testCase | testCase testSelectors do: [ :selector | allTests add: (testCase methodDict at: selector) methodReference fullName ] ].
	self logTruePositives: (smarttestAdvice intersection: oracle).
	self logFalsePositives: (smarttestAdvice difference: oracle).
	self logFalseNegatives: (oracle difference: smarttestAdvice).
	self logTrueNegatives: ((allTests difference: smarttestAdvice) difference: oracle)