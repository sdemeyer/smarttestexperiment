logging
closelog
	(logstream notNil and: [ logstream isStream ])
		ifTrue: [ logstream close ]