actions-public
createTestcases
	classesUnderTest := OrderedCollection new.
	testCaseClasses := OrderedCollection new.
	testResources := OrderedCollection new.
	packages
		do: [ :cat | 
			| package |
			package := cat asPackageIfAbsent: [ nil ].
			package notNil
				ifTrue: [ package classes
						do: [ :aClass | 
							aClass isTestCase
								ifTrue: [ testCaseClasses add: aClass ]
								ifFalse: [ aClass isManifest
										ifFalse: [ (aClass canUnderstand: #resources) & (aClass canUnderstand: #setUp) & (aClass canUnderstand: #tearDown)
												ifTrue: [ testResources add: aClass ]
												ifFalse: [ classesUnderTest add: aClass ] ] ] ] ] ].
	testSuite := TestSuite named: 'All test cases'.
	testCaseClasses do: [ :testCase | testCase addToSuiteFromSelectors: testSuite ].
	self logCreatedTestcases