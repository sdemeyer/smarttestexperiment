private
processChangeDryRun
	| smarttestAnalysis |
	currentChange isCodeChange
		ifTrue: [ self logAppliedCodeChange.
			currentChange applyCodeChange ]
		ifFalse: [ self logIgnoreCodeChange ]