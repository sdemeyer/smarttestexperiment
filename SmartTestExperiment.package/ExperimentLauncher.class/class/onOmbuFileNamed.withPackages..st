instance creation
onOmbuFileNamed: aFileName withPackages: packageNames
	"self onOmbuFileNamed: 'PharoLANTest.ombu' withPackages: {'LANSimulation-Tests'. 'LANSimulation-Internals'. 'LANSimulation'}"

	| fileRef logfileRef |
	fileRef := aFileName asFileReference.
	logfileRef := (fileRef absolutePath parent asFileReference / (fileRef base , '_LOG.txt')) asFileReference.
	lastRun := self new
		initialize;
		packages: packageNames;
		logFileReference: logfileRef;
		changeList: ((EpLog freshFromFile: fileRef) entries collect: [ :om | om content ]);
		yourself.
	^ lastRun