accessing
logFileReference: aFileReference
	aFileReference assert: aFileReference class == FileReference  description: 'Expected a FileReference for #logFileReference:'.
	logFileReference := aFileReference