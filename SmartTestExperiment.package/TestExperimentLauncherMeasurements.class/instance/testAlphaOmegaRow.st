running
testAlphaOmegaRow
	"self run: #testAlphaOmegaRow"

	| putString putType |
	putString := 'an EpClassAddition(Node)'.
	putType := 'Addition'.
	self assert: (measurements put: putString atColumn: #change) equals: putString.
	self assert: (measurements valueAtColumn: #change) equals: putString.
	self assert: (measurements put: putType atColumn: #changeType) equals: putType.
	self assert: (measurements valueAtColumn: #changeType) equals: putType.
	self assert: (measurements valueAtColumn: #falseNegatives) equals: 0.
	measurements incFalseNegatives: 3.
	self assert: (measurements valueAtColumn: #falseNegatives) equals: 3.
	self assert: (measurements incFalseNegatives: 5) equals: 8.
	self assert: (measurements valueAtColumn: #falseNegatives) equals: 8.
	self should: [ measurements valueAtColumn: #unexistingColumns ] raise: AssertionFailure description: 'Precondition on #valueAtColumn: did not trigger'.
	self should: [ measurements put: -1 atColumn: #unexistingColumns ] raise: AssertionFailure description: 'Precondition on #valueAtColumn: did not trigger'.
	self should: [ measurements inc: 2 atColumn: #unexistingColumns ] raise: AssertionFailure description: 'Precondition on #valueAtColumn: did not trigger'.
	^ measurements printString