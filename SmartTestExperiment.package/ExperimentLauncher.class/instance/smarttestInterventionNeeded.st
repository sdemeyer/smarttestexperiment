private
smarttestInterventionNeeded
		"Answer whether currentChange corresponds to a change which requires a Smarttest intervention"
	^(currentChange isEpMethodChange and: [ currentChange class ~= EpMethodRemoval ])