software visualization
createChartData: collectionOfPoints label: aLabel color: aColor
	| ds |
	ds := RTData new.
	ds barShape color: aColor.
	ds points: collectionOfPoints.
	ds y: [ :v | v ].
	ds label: aLabel. ds dotShape circle color: aColor.
	^ ds