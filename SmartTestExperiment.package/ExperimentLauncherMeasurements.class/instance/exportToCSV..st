public
exportToCSV: fileName
	| stream writer fileReference |
	fileReference := fileName asFileReference.
	fileReference exists
		ifTrue: [ fileReference delete ].
	[ stream := fileName asFileReference writeStream.
	writer := (NeoCSVWriter on: stream)
		separator: $;;
		nextPut: (columnNames collect: [:colName | self class convertCamelcaseToTitle: colName ]);
		yourself.
	writer nextPutAll: measurements ]
		ensure: [ stream close ]