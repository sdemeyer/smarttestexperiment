software visualization
visualisePrecisionStackBarPlot
	| b |
	b := RTStackBarPlot new.
	b add: (measurements collect: [ :row | row at: 5 ]) title: 'trueNegatives'.
	b add: (measurements collect: [ :row | row at: 2 ]) title: 'truePositives'.
	b add: (measurements collect: [ :row | row at: 3 ]) title: 'falsePositives'.
	b add: (measurements collect: [ :row | row at: 4 ]) title: 'falseNegatives'.
	"b axisX
		noLabel;
		noTick.
	b legend right."
	b inspect