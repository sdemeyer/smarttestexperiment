printing
printOn: aStream
	"| measurements |
	measurements := self new: 2.
	measurements put: 'an EpClassAddition(Node)' atColumn: #change.
	measurements put: 'Addition' atColumn: #changeType.
	measurements incFalseNegatives: 3.
	measurements startNewRow.
	measurements put: 'an EpClassModification(Node)' atColumn: #change.
	measurements put: 'Modification' atColumn: #changeType.
	measurements incFalsePositives: 5.
	measurements incFalseNegatives: 5.
	measurements incTruePositives: 5.
	measurements incTrueNegatives: 5.
	^ measurements printString"

	super printOn: aStream.
	aStream
		crtab;
		nextPutAll: 'true positives: '.
	truePositives printOn: aStream.
	aStream
		crtab;
		nextPutAll: 'false positives: '.
	falsePositives printOn: aStream.
	aStream
		crtab;
		nextPutAll: 'false negatives: '.
	falseNegatives printOn: aStream.
	aStream
		crtab;
		nextPutAll: 'true negatives: '.
	trueNegatives printOn: aStream.
	(columnNames notNil and: [ columnNames isSequenceable ])
		ifTrue: [ aStream cr.
			columnNames
				do: [ :columnName | 
					columnName printOn: aStream.
					aStream tab ] ].
	(measurements notNil and: [ measurements isSequenceable ])
		ifTrue: [ measurements
				do: [ :row | 
					aStream cr.
					row
						do: [ :cell | 
							cell printOn: aStream.
							aStream tab ] ] ]