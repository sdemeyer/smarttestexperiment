utilities
fixOmbuFileNamed: aFileName
	"Some Epicea files contain little quirks which hamper or even prevent launching a SmartTest experiment. This method goes over an epicea change file, fixes the issues and then overwrites the file so that future experimemt runs will go smoothly"

	"self fixOmbuFileNamed: 'PharoLANTest.ombu'"

	| inputLog outputLog changeList |
	inputLog := EpLog freshFromFile: aFileName asFileReference.
	changeList := inputLog entries asOrderedCollection.
	1 to: changeList size - 1 do: [ :inx | 
		(changeList at: inx) content class == EpBehaviorCommentChange
			ifTrue: [ (changeList at: inx + 1) content class == EpClassAddition
					ifTrue: [ | tmp |
						Transcript
							cr;
							show: 'Switched ' , (changeList at: inx) content printString , ' with ' , (changeList at: inx + 1) content printString.
						tmp := changeList at: inx.
						changeList at: inx put: (changeList at: inx + 1).
						changeList at: inx + 1 put: tmp ] ] ].
	outputLog := EpLog newWithStore: (OmStoreFactory current named: aFileName asFileReference base , '_FX' inDirectory: aFileName asFileReference absolutePath parent asFileReference).
	^ EpOmbuExporter new
		outputLog: outputLog;
		fileOut: changeList