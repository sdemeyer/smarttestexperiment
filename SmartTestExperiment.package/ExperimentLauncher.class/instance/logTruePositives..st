logging
logTruePositives: aSetOfMethodSelectors
	aSetOfMethodSelectors isEmpty
		ifTrue: [ self log: 'No true positives' crBefore: true indentation: 3 ]
		ifFalse: [ measurements incTruePositives: aSetOfMethodSelectors size. self log: '## true positives >> ' , aSetOfMethodSelectors size printString crBefore: true indentation: 3.
			self log: aSetOfMethodSelectors printString crBefore: true indentation: 4 ]