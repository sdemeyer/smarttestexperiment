I represent a launcher for excersising SmartTest using an Epicea change-list and collecting various metrics

Responsabilities:
* run: this runs a complete change-list and collects the measurements.
* oracle creation: creates an  mapping for each class which tests excercise it. Used as an oracle for calculating false positives, false negatives, ...
* fixing: some epice files contain problematic constructs. At the class level there are a few utilities to clean them up
* loging: I have a class level switch to enable/disable logging

Collaborators Part:
* Epicea: I relie heavily on the class hierarchy and the protocol to know which type of changes are under analysis and the way to apply them
* I rely on the SmartTest finder and the annunce protocol (SmTListener) , to see which changes actually mapped onto tests
* I rely on SUnit and TestSuite to calculate the test coverage while creating the oracle


Public API and Key Messages
* See #runCreationOfOracle for a typical scenario

Internal Representation and Key Implementation Points.
... to be done