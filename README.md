# README #

This is a Pharo plug-in to run a scientific experiment with *SmartTest*. It stimulates the tool and meanwhile collects measurements for subsequent analysis.

## What is SmartTest ? ##

*SmartTest* is an *pro-active* test execution tool. It is a little utility that sits in the background of your code browser,
silently monitoring the changes you make, and running the [SUnit] (https://en.wikipedia.org/wiki/SUnit)tests that are affected.

* [The tool itself is in the GitHub Repository](http://badetitou.github.io/research/smalltalk/2017/08/21/SmartTest/)
* [An award winning video](https://youtu.be/jAvfdN2z5-s)

## What is SmartTestExperiment ? ##

We want to conduct a feasibility study with *SmartTest*; demonstrating that the tool is capable of providing actionable suggestions (running only a few SUnit tests) in a responsive way (answer within milliseconds !).

To do that, the experiments replays changes from past development activities collecting a series of measurements, using the following steps, also shown in Figure 1.

1. We load the changes from [epicea](http://smalltalkhub.com/#!/~MartinDias/Epicea) (or [monticello](http://book.seaside.st/book/getting-started/pharo/monticello) or [iceberg](https://github.com/pharo-vcs/iceberg) for that matter).
2. We create an *oracle*, identifying the minimal test suite that covers each of the changes; meanwhile colecting the [test coverage] (https://en.wikipedia.org/wiki/Code_coverage) (*function coverage* to be precise).
3. We collect the measurements: the false negatives (we should have zero), the false positives (we tolerate a few), test execution time (should be small), test reduction (should be large).

![Figure 1: Overview of the experiment](experiment_setup.jpg)

*Figure 1: Overview of the experiment*

## What data do we need ? ##

We are looking for change logs from realistic development projects which heavily rely on [SUnit](https://en.wikipedia.org/wiki/SUnit). 

What do we need ?

1. The changes files themselves:
    * epicea: the .ombu files stored in pharo-local/ombu-sessions
    * monticello: the .mcz files stored in your monticello repositories
    * iceberg: read access to your repository
    * (!! Note that for the monticello and iceberg options, we will recreate the epicea files by loading the files in a clean image.)
2. A way to recreate the starting image (i.e which packages must loaded, which configuration files must be set, which databases should be available, ...).
3. A little explanation of your project (what is the goal ? why did you start it ? who was involved ?)
4. A little explanation of how you used SUnit (strictly unit tests, some component tests, scenario tests, integration tests, ...)