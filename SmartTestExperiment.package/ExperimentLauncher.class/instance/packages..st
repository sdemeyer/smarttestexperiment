accessing
packages: aCollectionOfCategoryNames
	aCollectionOfCategoryNames assert: aCollectionOfCategoryNames isSequenceable description: 'Expected a collection for #packages:'.
	packages := aCollectionOfCategoryNames