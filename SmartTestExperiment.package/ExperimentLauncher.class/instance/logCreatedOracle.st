logging
logCreatedOracle
	| currentOracle |
	self log: 'Oracle Methodundertest -> Testcase: --- ' crBefore: true indentation: 2.
	currentChange isNil
		ifTrue: [ mapMethodundertestToTestcase
				keysAndValuesDo: [ :key :value | 
					self log: key asString crBefore: true indentation: 3.
					self log: value printString crBefore: true indentation: 4 ] ]
		ifFalse: [ currentOracle := mapMethodundertestToTestcase at: currentChange methodAffected fullName ifAbsent: [ {'No test case found'} ].
self log: currentChange methodAffected fullName asString crBefore: true indentation: 3.			self log: currentOracle printString crBefore: true indentation: 4 ]