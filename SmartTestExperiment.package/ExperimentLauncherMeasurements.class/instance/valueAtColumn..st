public
valueAtColumn: columnName
	self assert: (columnNames includes: columnName) description: 'Wrong columnName ' , columnName printString.
	^ measurements last at: (columnNames indexOf: columnName)