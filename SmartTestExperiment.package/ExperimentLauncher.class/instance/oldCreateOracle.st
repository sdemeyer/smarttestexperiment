actions-public
oldCreateOracle
	"self runCreationOfOracle"

	| wrappers singleTestcaseSuite singleTestcaseWrapper |
	wrappers := self createWrappersForClassesUnderTest: classesUnderTest.
	testCaseClasses
		do: [ :testCase | 
			testCase testSelectors
				do: [ :selector | 
					self addToTestResultCovered: (testCase methodDict at: selector) methodReference byTestcase: testCase selector: selector.	"Add the trivial coverega info: the test case affects itself"
					singleTestcaseSuite := TestSuite named: 'Single Test Case for ' , selector.
					singleTestcaseSuite addTest: (testCase selector: selector).
					singleTestcaseWrapper := wrappers copy.
					[ singleTestcaseWrapper do: [ :each | each install ].
					[ singleTestcaseSuite run ]
						ensure: [ singleTestcaseWrapper do: [ :each | each uninstall ] ] ] valueUnpreemptively.
					singleTestcaseWrapper
						do: [ :testCoverage | 
							testCoverage hasRun
								ifTrue: [ self addToTestResultCovered: testCoverage reference byTestcase: testCase selector: selector ] ] ] ].
	self logCreatedOracle