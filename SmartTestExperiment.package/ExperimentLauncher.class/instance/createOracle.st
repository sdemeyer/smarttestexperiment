actions-public
createOracle
	"self runCreationOfOracle"

	| singleTestcaseSuite singleTestcaseWrapper |
	currentChange isNil
		ifTrue: [ | inx |
			inx := changeList findLast: [ :anEpChange | anEpChange class = EpMethodAddition | (anEpChange class = EpMethodModification) and: [ anEpChange methodAffected compiledMethod notNil ] ].
			currentChange := changeList at: inx ].
	testCaseClasses
		do: [ :testCase | 
			testCase testSelectors
				do: [ :selector | 
					"self addToTestResultCovered: (testCase methodDict at: selector) methodReference byTestcase: testCase selector: selector.	Add the trivial coverega info: the test case affects itself"
					singleTestcaseSuite := TestSuite named: 'Single Test Case for ' , selector.
					singleTestcaseSuite addTest: (testCase selector: selector).
					singleTestcaseWrapper := OrderedCollection with: (TestCoverage on: currentChange methodAffected compiledMethod methodReference).
					[ singleTestcaseWrapper do: [ :each | each install ].
					[ singleTestcaseSuite run ]
						ensure: [ singleTestcaseWrapper do: [ :each | each uninstall ] ] ] valueUnpreemptively.
					singleTestcaseWrapper
						do: [ :testCoverage | 
							testCoverage hasRun
								ifTrue: [ self addToTestResultCovered: testCoverage reference byTestcase: testCase selector: selector ] ] ] ].
	self logCreatedOracle