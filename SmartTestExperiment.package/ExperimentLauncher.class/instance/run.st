actions-public
run
	"self runTestScenario"

	self log: '-- Launching Smarttest experiment' crBefore: false indentation: 0.
	UIManager default
		displayProgress: 'Launching Smart Test Experiment'
		from: 1
		to: changeList size * 2
		during: [ :bar | 
			changeList
				do: [ :anEpCodeChange | 
					currentChange := anEpCodeChange.
					self smarttestInterventionNeeded
						ifTrue: [ self resetCollectingResults ].
					self logStartOfCodeChange.
					bar
						increment;
						label: 'Loading change for ' , anEpCodeChange printString.
					self processChange.
					self collectMeasurementsCodechurn.
					bar
						increment;
						label: 'Create oracle for ' , anEpCodeChange printString.
					self smarttestInterventionNeeded
						ifTrue: [ self createTestcases.
							self createOracle.
							self collectMeasurementsPrecision.
							self closeRowOfResults ] ] ].
	measurements logCollectedMeasurementsForLauncher: self